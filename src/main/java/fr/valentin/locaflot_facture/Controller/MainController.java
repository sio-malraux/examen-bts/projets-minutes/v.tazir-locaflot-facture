package fr.valentin.locaflot_facture.Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import fr.valentin.locaflot_facture.View.MainView;
import fr.valentin.locaflot_facture.DAO.RentalAgreementDAO;
import fr.valentin.locaflot_facture.DAO.BoatTypeDAO;
import fr.valentin.locaflot_facture.App;
import fr.valentin.locaflot_facture.Classes.Rent;
import fr.valentin.locaflot_facture.Classes.RentalAgreement;
import fr.valentin.locaflot_facture.DAO.BoatDAO;
import fr.valentin.locaflot_facture.DAO.RentDAO;

/**
 * MainController : Controller of the project.
 * @author Valentin Tazir
 */
public class MainController implements ActionListener
{
	private MainView view;
	private static ResourceBundle labels = ResourceBundle.getBundle("Labels");
	private RentalAgreementDAO contract_manager;
	private BoatTypeDAO boat_type_manager;
	private BoatDAO boat_manager;
	private RentDAO rent_manager;
	
	/**
	 * Unique constructor which initialize all the DAO objects.
	 */
	public MainController()
	{
		this.contract_manager = new RentalAgreementDAO();
		this.boat_type_manager = new BoatTypeDAO();
		this.boat_manager = new BoatDAO();
		this.rent_manager = new RentDAO();
	}

	/**
	 * Manage the onClick events on the buttons
	 */
	public void actionPerformed(ActionEvent e)
	{
		if (e.getActionCommand().equals("Generate Bill"))
		{
			App.logger.debug("Button 'Generate Bill' clicked");
			int selected_contract_id = view.getContractSelection();
			if(selected_contract_id != -1)
			{
				view.ShowBill(rent_manager.recupAllRentOfContract(selected_contract_id));
			}	
		}
		else if (e.getActionCommand().equals("Return"))
		{
			App.logger.debug("Button 'Return' clicked");
			try
			{
				view.ShowContracts(contract_manager.recupAll());
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
			view.ShowHomepage();
		}
		else if (e.getActionCommand().equals("PDF"))
		{
			App.logger.debug("Button 'PDF' clicked");
			int selected_contract_id = view.getContractSelection();
			this.generatePDF(rent_manager.recupAllRentOfContract(selected_contract_id), selected_contract_id);
		}
	}
	
	/**
	 * Set the view to show and populate the combo box with the contract infos.
	 * @param v : The view to show.
	 */
	public void setView(MainView v)
	{
		this.view = v;
		List<RentalAgreement> all_rental_agreements = contract_manager.recupAll();
		try {
			view.ShowContracts(all_rental_agreements);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	private void generatePDF(List<Rent> rent_list, int selected_contract_id)
	{
		RentalAgreement contract = rent_list.get(0).getContract();
		try
		{
			App.logger.debug("Generating PDF...");
			Document doc = new Document();
			this.verifyFolder();
			PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream(new MessageFormat(labels.getString("pdfOutputPath")).format(new Object[] {selected_contract_id})));
			doc.open();
			double total_amount = 0.0;
			Paragraph title = new Paragraph(new MessageFormat(labels.getString("pdfTitle")).format(new Object[] {selected_contract_id}) + "\n\n\n", new Font(Font.HELVETICA, 25, Font.BOLDITALIC));
			title.setAlignment("center");
			title.setExtraParagraphSpace(25);
			doc.add(title);
			
			Paragraph contract_timestamp = new Paragraph(new MessageFormat(labels.getString("pdfContractTimestamp")).format(new Object[] {new SimpleDateFormat("yyyy-MM-dd").parse(contract.getDate()), new SimpleDateFormat("HH:mm:ss").parse(contract.getBegin_hour()), new SimpleDateFormat("HH:mm:ss").parse(contract.getEnd_hour())}) + "\n\n");
			doc.add(contract_timestamp);
			if (rent_list.size() > 0)
			{
				App.logger.debug("Creating table");
				total_amount = this.createTable(rent_list, contract, doc);
			}
			else
			{
				App.logger.warn("Contract has no rents, no table generated");
			}
			Paragraph contract_nb_boats = new Paragraph(new MessageFormat(labels.getString("pdfNbBoats")).format(new Object[] {rent_list.size()}));
			contract_nb_boats.setAlignment("left");
			doc.add(contract_nb_boats);
			
			Paragraph contract_total_amount = new Paragraph(new MessageFormat(labels.getString("pdfTotalAmount")).format(new Object[] {total_amount}));
			contract_total_amount.setAlignment("left");
			doc.add(contract_total_amount);
			App.logger.debug("PDF generation end");
			doc.close();
			writer.close();
			JOptionPane.showMessageDialog(null, labels.getString("pdfPopUp"));
		} catch (DocumentException | FileNotFoundException e) {
			App.logger.fatal("PDF Generation error : " + e.getMessage());
		} catch (ParseException e) {
			App.logger.fatal("Date-Time parsing error : " + e.getMessage());
		}
	}

	private void verifyFolder() {
		App.logger.debug("Verifying pdf folder existence");
		File folder = new File("./pdf/");
		if(folder.mkdir())
		{
			App.logger.warn("Folder pdf doesn't exist, creating a new one.");
		}
		else
		{
			App.logger.info("Folder pdf already exist.");
		}
	}
	
	private double createTable(List<Rent> rent_list, RentalAgreement contract, Document doc)
	{
		double total_amount = 0.0;
		PdfPTable table = new PdfPTable(3);
		Font font = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);
		App.logger.debug("Creating header rows");
		PdfPCell cell = new PdfPCell();
		cell.setPhrase(new Phrase(labels.getString("pdfBoatId"), font));
		table.addCell(cell);
		cell.setPhrase(new Phrase(labels.getString("pdfBoatType"), font));
		table.addCell(cell);
		cell.setPhrase(new Phrase(labels.getString("pdfAmount"), font));
		table.addCell(cell);
		App.logger.debug("Populating body rows");
		for(Rent r : rent_list)
		{
			table.addCell(r.getRented_boat().getId());
			table.addCell(r.getRented_boat().getType().getName());
			double amount = view.getBoatRentPrice(contract, r.getRented_boat().getType());
			table.addCell(Double.toString(amount) + " euros");
			total_amount += amount;
		}
		doc.add(table);
		return total_amount;
	}
}
