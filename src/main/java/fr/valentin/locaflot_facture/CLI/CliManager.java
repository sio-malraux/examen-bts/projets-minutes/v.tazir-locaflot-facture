package fr.valentin.locaflot_facture.CLI;

import org.apache.commons.cli.*;

import fr.valentin.locaflot_facture.App;

/**
 * Class used to manage the options on the command line
 * @author Valentin
 */
public class CliManager
{
	private static String locale;
	private static final Options options = new Options();
	
	/**
	 * Unique constructor which process the command line parameters.
	 * @param args The arguments on the command line, available on the main.
	 */
	public CliManager(String[] args)
	{
		HandleOptions();
		
		App.logger.debug("Parsing command line");
		CommandLineParser parser = new DefaultParser();
		try
		{
			CommandLine cmd = parser.parse(options, args);
			ProcessOptions(cmd);
		}
		catch (ParseException e)
		{
			App.logger.warn("Command line parsing error : " + e.getMessage() + "\n'Locale' option set null");
			PrintHelp();
			locale = null;
		}
	}
	
	/**
	 * Get the argument of the CLI 'locale' option.
	 * @return The 'locale' option parameter
	 */
	public String getLocale()
	{
		return locale;
	}
	
	/**
	 * Print the help message of the app.
	 */
	public void PrintHelp()
	{
		// automatically generate the help statement
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp( "locaflot-facture-0.0.1-SNAPSHOT.jar", options, true);
		App.logger.debug("Help message printed, closing app.");
		System.exit(0);
	}
	
	private void ProcessOptions(CommandLine cmd)
	{
		if(cmd.hasOption("h") || cmd.hasOption("help"))
		{
			App.logger.debug("'help' option typed.");
			PrintHelp();
		}
		else if(cmd.hasOption("l") || cmd.hasOption("locale"))
		{
			App.logger.debug("'locale' option typed.");
			locale = cmd.getOptionValue("l");
			if(locale == null)
			{
				locale = cmd.getOptionValue("locale");
			}
		}
	}
	
	private void HandleOptions()
	{
		App.logger.debug("Setting up 'locale' option.");
		Option l = new Option("l", "locale", true, "Language used for the HMI.\nAvailable options : FR, French.\nDefault : English.");
		l.setArgs(1);
		l.setArgName("langage");
		l.setRequired(false);
		options.addOption(l);
		
		App.logger.debug("Setting up 'help' option.");
		Option h = new Option("h", "help", false, "Print this message.");
		h.setRequired(false);
		options.addOption(h);
	}
}
