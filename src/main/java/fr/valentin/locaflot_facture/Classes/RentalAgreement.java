package fr.valentin.locaflot_facture.Classes;

/**
 * RentalAgreement : Describe a rental contract
 * @author Valentin Tazir
 */
public class RentalAgreement
{
	private int id;
	private String date;
	private String begin_hour;
	private String end_hour;
	
	/**
	 * Get the id of the rental agreement.
	 * @return id : Id of the rental agreement.
	 */
	public int getId()
	{
		return id;
	}
	
	/**
	 * Mutate the id attribute with the value of the parameter.
	 * @param id : The new id of the rental agreement.
	 */
	public void setId(int id)
	{
		this.id = id;
	}
	
	/**
	 * Get the date of the rental agreement.
	 * @return date : Date of the rental agreement.
	 */
	public String getDate()
	{
		return date;
	}
	
	/**
	 * Mutate the date attribute with the value of the parameter.
	 * @param date : The new date of the rental agreement.
	 */
	public void setDate(String date)
	{
		this.date = date;
	}
	
	/**
	 * Get the start time of the rent.
	 * @return begin_hour : the start time of the rental agreement.
	 */
	public String getBegin_hour()
	{
		return begin_hour;
	}
	
	/**
	 * Mutate the begin hour attribute with the value of the parameter.
	 * @param begin_hour : The new start time of the rental agreement.
	 */
	public void setBegin_hour(String begin_hour)
	{
		this.begin_hour = begin_hour;
	}
	
	/**
	 * Get the stop time of the rent.
	 * @return end_hour : the stop time of the rental agreement.
	 */
	public String getEnd_hour()
	{
		return end_hour;
	}
	
	/**
	 * Mutate the end hour attribute with the value of the parameter.
	 * @param end_hour : The new stop time of the rental agreement
	 */
	public void setEnd_hour(String end_hour)
	{
		this.end_hour = end_hour;
	}

	/**
	 * Default constructor without parameters.
	 */
	public RentalAgreement()
	{
		this.id = 0;
		this.date = null;
		this.begin_hour = null;
		this.end_hour = null;
	}
	
	/**
	 * Constructor with parameters to initialize the attributes.
	 * @param id : Id of the rental agreement.
	 * @param date : Date of the rent.
	 * @param begin_hour : Start hour of the rent.
	 * @param end_hour : Stop hour of the rent.
	 */
	public RentalAgreement(int id, String date, String begin_hour, String end_hour)
	{
		this.id = id;
		this.date = date;
		this.begin_hour = begin_hour;
		this.end_hour = end_hour;
	}

	/**
	 * @return A String containing all the informations concerning the RentalAgreement object.
	 */
	@Override
	public String toString()
	{
		return "Rental Agreement id : " + id 
				+ "\ndate : " + date 
				+ "\nbegin_hour : " + begin_hour 
				+ "\nend_hour : " + end_hour;
	}	
}
