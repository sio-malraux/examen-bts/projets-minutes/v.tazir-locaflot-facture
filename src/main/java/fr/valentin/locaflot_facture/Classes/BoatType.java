package fr.valentin.locaflot_facture.Classes;

/**
 * BoatType : Represent a boat type.
 * @author Valentin
 */
public class BoatType
{
	private String code;
	private String name;
	private int seating_capacity;
	private double half_hourly_price;
	private double hourly_price;
	private double half_daily_price;
	private double daily_price;
	
	/** 
	 * Get the code of the boat type.
	 * @return code : Code of the boat type.
	 */
	public String getCode()
	{
		return code;
	}
	
	/**
	 * Mutate the code attribute with the value of the parameter.
	 * @param code : The new code of the boat type.
	 */
	public void setCode(String code)
	{
		this.code = code;
	}
	
	/**
	 * Get the name of the boat type.
	 * @return name : The name of the boat type.
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Mutate the name attribute with the value of the parameter.
	 * @param name : The new name of the boat type.
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	
	/** 
	 * Get the seating capacity of the boat type.
	 * @return seating_capacity : Number of places available of the boat type.
	 */
	public int getSeatingCapacity()
	{
		return seating_capacity;
	}
	
	/**
	 * Mutate the seating capacity attribute with the value of the parameter.
	 * @param seating_capacity : The new number of places available of the boat type.
	 */
	public void setSeatingCapacity(int seating_capacity)
	{
		this.seating_capacity = seating_capacity;
	}
	
	/** 
	 * Get the half hourly amount of the boat type.
	 * @return half_hourly_price : Price per half hour of boat type rental.
	 */
	public double getHalfHourlyPrice()
	{
		return half_hourly_price;
	}
	
	/**
	 * Mutate the half hourly price attribute with the value of the parameter.
	 * @param half_hourly_price : The new price per half hour of boat type rental.
	 */
	public void setHalfHourlyPrice(double half_hourly_price)
	{
		this.half_hourly_price = half_hourly_price;
	}
	
	/** 
	 * Get the hourly amount of the boat type.
	 * @return hourly_price : Price per hour of boat type rental.
	 */
	public double getHourlyPrice()
	{
		return hourly_price;
	}
	
	/**
	 * Mutate the hourly price attribute with the value of the parameter.
	 * @param hourly_price : The new price per hour of boat type rental.
	 */
	public void setHourlyPrice(double hourly_price)
	{
		this.hourly_price = hourly_price;
	}
	
	/** 
	 * Get the half daily amount of the boat type.
	 * @return half_daily_price : Price per half day of boat type rental.
	 */
	public double getHalfDailyPrice()
	{
		return half_daily_price;
	}
	
	/**
	 * Mutate the half daily price attribute with the value of the parameter.
	 * @param half_daily_price : The new price per half day of boat type rental.
	 */
	public void setHalfDailyPrice(double half_daily_price)
	{
		this.half_daily_price = half_daily_price;
	}
	
	/** 
	 * Get the daily amount of the boat type.
	 * @return daily_price : Price per day of boat type rental.
	 */
	public double getDailyPrice()
	{
		return daily_price;
	}
	
	/**
	 * Mutate the daily price attribute with the value of the parameter.
	 * @param daily_price : The new price per day of boat type rental.
	 */
	public void setDailyPrice(double daily_price)
	{
		this.daily_price = daily_price;
	}

	/**
	 * Default constructor without parameters
	 */
	public BoatType()
	{
		this.code = null;
		this.name = null;
		this.seating_capacity = 0;
		this.half_hourly_price = 0;
		this.hourly_price = 0;
		this.half_daily_price = 0;
		this.daily_price = 0;
	}
	
	/**
	 * Constructor with parameters to initialize the attributes.
	 * @param code : Code of the boat type.
	 * @param name : Name of the boat type.
	 * @param seating_capacity : Number of places available of the boat type.
	 * @param half_hourly_price : Price per half hour of rent.
	 * @param hourly_price : Price per hour of rent.
	 * @param half_daily_price : Price per half day of rent.
	 * @param daily_price : price per day of rent.
	 */
	public BoatType(String code, String name, int seating_capacity, double half_hourly_price, double hourly_price,
			double half_daily_price, double daily_price)
	{
		this.code = code;
		this.name = name;
		this.seating_capacity = seating_capacity;
		this.half_hourly_price = half_hourly_price;
		this.hourly_price = hourly_price;
		this.half_daily_price = half_daily_price;
		this.daily_price = daily_price;
	}
	
	/**
	 * @return A String containing all the informations concerning the BoatType object.
	 */
	@Override
	public String toString()
	{
		return "code : " + code + "\nname : " + name 
				+ "\nseating capacity : " + seating_capacity
				+ "\nhalf-hourly price : " + half_hourly_price 
				+ " euros\nhourly price : " + hourly_price
				+ " euros\nhalf-daily price : " + half_daily_price
				+ " euros\ndaily price : " + daily_price + " euros";
	}
}
