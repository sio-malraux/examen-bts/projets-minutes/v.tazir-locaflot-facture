package fr.valentin.locaflot_facture.Classes;

import fr.valentin.locaflot_facture.Classes.BoatType;

/**
 * Boat : Represent a boat.
 * @author Valentin Tazir
 */
public class Boat
{
	private String id;
	private String color;
	private boolean available;
	private BoatType type;
	
	/** 
	 * Get the id of the boat.
	 * @return id : Id of the boat.
	 */
	public String getId()
	{
		return id;
	}
	
	/**
	 * Mutate the id attribute with the value of the parameter.
	 * @param id : The new id of the Boat.
	 */
	public void setId(String id)
	{
		this.id = id;
	}
	
	/** 
	 * Get the color of the boat.
	 * @return color : Color of the boat.
	 */
	public String getColor()
	{
		return color;
	}
	
	/**
	 * Mutate the color attribute with the value of the parameter.
	 * @param color : The new color of the Boat.
	 */
	public void setColor(String color)
	{
		this.color = color;
	}
	
	/** 
	 * Get the availability of the boat.
	 * @return available : True if the boat is available to be rented, false otherwise.
	 */
	public boolean isAvailable()
	{
		return available;
	}
	
	/**
	 * Mutate the available attribute with the value of the parameter.
	 * @param available : true if the boat is available to be rented, false otherwise.
	 */
	public void setAvailable(boolean available)
	{
		this.available = available;
	}
	
	/** 
	 * Get the BoatType object of the boat.
	 * @return type : type of the boat.
	 */
	public BoatType getType()
	{
		return type;
	}
	
	/**
	 * Mutate the type attribute with the value of the parameter.
	 * @param type : The new boat type of the boat.
	 */
	public void setType(BoatType type)
	{
		this.type = type;
	}
	
	/**
	 * Default constructor without parameters.
	 */
	public Boat() {
		this.id = null;
		this.color = null;
		this.available = false;
		this.type = new BoatType();
	}

	/**
	 * Constructor with parameters to initialize the attributes.
	 * @param id : Id of the boat.
	 * @param color : color of the boat.
	 * @param available : true if the boat is available to be rented, false otherwise. 
	 * @param type : BoatType object representing the type of the boat.
	 */
	public Boat(String id, String color, boolean available, BoatType type)
	{
		this.id = id;
		this.color = color;
		this.available = available;
		this.type = type;
	}

	/**
	 * @return A String containing all the informations concerning the Boat object.
	 */
	@Override
	public String toString()
	{
		return "Boat id : " + id
				+ "\ncolor=" + color
				+ "\navailable=" + available
				+ "\nBoat type infos : \n" + type.toString();
	}	
}
