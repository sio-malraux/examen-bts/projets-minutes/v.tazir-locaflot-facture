package fr.valentin.locaflot_facture.Classes;

import fr.valentin.locaflot_facture.Classes.Boat;
import fr.valentin.locaflot_facture.Classes.RentalAgreement;

/**
 * Rent : Describe a rent 
 * @author Valentin Tazir
 */
public class Rent
{
	private RentalAgreement contract;
	private Boat rented_boat;
	private int nb_persons;
	
	/**
	 * Get the RentalAgreement object of the rent.
	 * @return contract : The rent contrat related to the rent.
	 */
	public RentalAgreement getContract()
	{
		return contract;
	}
	
	/**
	 * Mutate the contract attribute with the value of the parameter.
	 * @param contract : The new rent contract of the rent.
	 */
	public void setContract(RentalAgreement contract)
	{
		this.contract = contract;
	}
	
	/**
	 * Get the Boat object of the rent.
	 * @return rented_boat : The boat rented.
	 */
	public Boat getRented_boat()
	{
		return rented_boat;
	}
	
	/**
	 * Mutate the rented boat attribute with the value of the parameter.
	 * @param rented_boat : The new boat rented.
	 */
	public void setRented_boat(Boat rented_boat)
	{
		this.rented_boat = rented_boat;
	}
	
	/**
	 * Get the number of persons concerned by the rent.
	 * @return nb_persons : The number of persons using the boat for this rent.
	 */
	public int getNb_persons()
	{
		return nb_persons;
	}
	
	/**
	 * Mutate the rented boat attribute with the value of the parameter.
	 * @param nb_persons : The new number of persons using the boat for this rent.
	 */
	public void setNb_persons(int nb_persons)
	{
		this.nb_persons = nb_persons;
	}
	
	/**
	 * Default constructor without parameters.
	 */
	public Rent()
	{
		this.contract = new RentalAgreement();
		this.rented_boat = new Boat();
		this.nb_persons = 0;
	}

	/**
	 * Constructor with parameters to initialize the attributes.
	 * @param contract : The RentalAgreement object related to the rent.
	 * @param rented_boat : The Boat object related to the rent.
	 * @param nb_persons : The number of customers using the boat during the rent.
	 */
	public Rent(RentalAgreement contract, Boat rented_boat, int nb_persons)
	{
		this.contract = contract;
		this.rented_boat = rented_boat;
		this.nb_persons = nb_persons;
	}

	/**
	 * @return A String containing all the informations concerning the Rent object.
	 */
	@Override
	public String toString()
	{
		return "Contract infos :\n" + contract.toString() 
				+ "\n\nRented boat infos :\n" + rented_boat 
				+ "\n\nNumber of persons : " + nb_persons;
	}	
}
