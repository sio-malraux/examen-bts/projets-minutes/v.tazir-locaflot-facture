package fr.valentin.locaflot_facture.View;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import fr.valentin.locaflot_facture.App;
import fr.valentin.locaflot_facture.Classes.BoatType;
import fr.valentin.locaflot_facture.Classes.Rent;
import fr.valentin.locaflot_facture.Classes.RentalAgreement;
import fr.valentin.locaflot_facture.Controller.MainController;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;

/**
 * MainView : The view of the project
 * @author Valentin Tazir
 */
public class MainView extends JFrame
{
	/**
	 * controller : used to listen the buttons.
	 */
	protected MainController controller;
	private static ResourceBundle labels = ResourceBundle.getBundle("Labels");
	private JPanel contentPane;
	private JTextField textFieldBegin;
	private JTextField textFieldEnd;
	private JTextField textFieldBoatNumber;
	private JTextField textFieldAmount;
	private JLabel lblContractSelection;
	private JComboBox comboBoxContracts;
	private JLabel lblLocationBegin;
	private JLabel lblLocationEnd;
	private JButton btnGenerateBill;
	private JTextArea textAreaBoatsInfo;
	private JLabel lblBoatNumber;
	private JLabel lblAmount;
	private JButton btnReturn;
	private JButton btnPdf;
	
	/**
	 * Create the frame and listen to the buttons.
	 * @param controller : The controller listening to the buttons.
	 */
	public MainView(MainController controller)
	{
		App.logger.info("Setting up the view");
		this.setResizable(false);
		this.setVisible(true);
		this.controller = controller;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 678, 430);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblContractSelection = new JLabel(labels.getString("lblContractSelection") + " :");
		lblContractSelection.setBounds(217, 124, 191, 16);
		contentPane.add(lblContractSelection);
		
		comboBoxContracts = new JComboBox();
		comboBoxContracts.setBounds(137, 165, 326, 22);
		contentPane.add(comboBoxContracts);
		
		lblLocationBegin = new JLabel(labels.getString("lblLocationBegin"));
		lblLocationBegin.setVisible(false);
		lblLocationBegin.setBounds(12, 23, 124, 16);
		contentPane.add(lblLocationBegin);
		
		textFieldBegin = new JTextField();
		textFieldBegin.setEditable(false);
		textFieldBegin.setVisible(false);
		textFieldBegin.setBounds(137, 20, 162, 22);
		contentPane.add(textFieldBegin);
		textFieldBegin.setColumns(10);
		
		lblLocationEnd = new JLabel(labels.getString("lblLocationEnd"));
		lblLocationEnd.setVisible(false);
		lblLocationEnd.setBounds(329, 23, 107, 16);
		contentPane.add(lblLocationEnd);
		
		btnGenerateBill = new JButton(labels.getString("btnGenerateBill"));
		btnGenerateBill.setBounds(217, 214, 172, 25);
		btnGenerateBill.setActionCommand("Generate Bill");
		btnGenerateBill.addActionListener(controller);
		contentPane.add(btnGenerateBill);
		
		textFieldEnd = new JTextField();
		textFieldEnd.setEditable(false);
		textFieldEnd.setVisible(false);
		textFieldEnd.setBounds(432, 20, 162, 22);
		contentPane.add(textFieldEnd);
		textFieldEnd.setColumns(10);
		
		textAreaBoatsInfo = new JTextArea();
		textAreaBoatsInfo.setEditable(false);
		textAreaBoatsInfo.setVisible(false);
		textAreaBoatsInfo.setBounds(63, 68, 507, 210);
		contentPane.add(textAreaBoatsInfo);
		
		lblBoatNumber = new JLabel(labels.getString("lblBoatNumber"));
		lblBoatNumber.setVisible(false);
		lblBoatNumber.setBounds(51, 291, 152, 16);
		contentPane.add(lblBoatNumber);
		
		textFieldBoatNumber = new JTextField();
		textFieldBoatNumber.setEditable(false);
		textFieldBoatNumber.setVisible(false);
		textFieldBoatNumber.setBounds(204, 288, 56, 22);
		contentPane.add(textFieldBoatNumber);
		textFieldBoatNumber.setColumns(10);
		
		lblAmount = new JLabel(labels.getString("lblAmount"));
		lblAmount.setVisible(false);
		lblAmount.setBounds(351, 291, 85, 16);
		contentPane.add(lblAmount);

		textFieldAmount = new JTextField();
		textFieldAmount.setEditable(false);
		textFieldAmount.setVisible(false);
		textFieldAmount.setBounds(441, 288, 116, 22);
		contentPane.add(textFieldAmount);
		textFieldAmount.setColumns(10);
		
		btnReturn = new JButton(labels.getString("btnReturn"));
		btnReturn.setVisible(false);
		btnReturn.setBounds(255, 357, 97, 25);
		btnReturn.setActionCommand("Return");
		btnReturn.addActionListener(controller);
		contentPane.add(btnReturn);
		
		btnPdf = new JButton("PDF");
		btnPdf.setVisible(false);
		btnPdf.setBounds(255, 323, 97, 25);
		btnPdf.setActionCommand("PDF");
		btnPdf.addActionListener(controller);
		contentPane.add(btnPdf);
	}
	
	//---------------------------- Methods used to show/hide the components depending on the selected menu -----------------------------
	
	/**
	 * Show the homepage by setting component visible or not.
	 */
	public void ShowHomepage()
	{
		App.logger.info("Showing homepage");
		textFieldBegin.setVisible(false);
		textFieldEnd.setVisible(false);
		textFieldBoatNumber.setVisible(false);
		textFieldAmount.setVisible(false);
		lblLocationBegin.setVisible(false);
		lblLocationEnd.setVisible(false);
		textAreaBoatsInfo.setVisible(false);
		lblBoatNumber.setVisible(false);
		lblAmount.setVisible(false);
		btnReturn.setVisible(false);
		btnPdf.setVisible(false);
		
		lblContractSelection.setVisible(true);
		comboBoxContracts.setVisible(true);
		btnGenerateBill.setVisible(true);
	}
	
	/**
	 * Show the bill page by setting component visible or not + populate the fields depending on the rents given in parameter.
	 * @param rent_list : List of rents related to the bill.
	 */
	public void ShowBill(List<Rent> rent_list)
	{
		App.logger.info("Showing the bill view");
		textFieldBegin.setVisible(true);
		textFieldEnd.setVisible(true);
		textFieldBoatNumber.setVisible(true);
		textFieldAmount.setVisible(true);
		lblLocationBegin.setVisible(true);
		lblLocationEnd.setVisible(true);
		textAreaBoatsInfo.setVisible(true);
		lblBoatNumber.setVisible(true);
		lblAmount.setVisible(true);
		btnReturn.setVisible(true);
		btnPdf.setVisible(true);
		
		lblContractSelection.setVisible(false);
		comboBoxContracts.setVisible(false);
		btnGenerateBill.setVisible(false);

		this.populateTextFields(rent_list);
	}
	
	//--------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Populate the drop down list with all the contract given in parameter. 
	 * @param contract_list : The list of RentalAgreement objects to show in the comboBox.
	 * @throws ParseException If getDate and get*_hour methods of RentalAgreement doesn't match patterns
	 */
	public void ShowContracts(List<RentalAgreement> contract_list) throws ParseException
	{
		App.logger.debug("Removing all items of the comboBox");
		comboBoxContracts.removeAllItems();
		App.logger.debug("Adding contract to the comboBox, number of contracts in the list : " + contract_list.size());
		comboBoxContracts.addItem(labels.getString("lblContractSelection"));
		
		// Internationalization message formatter
		MessageFormat formatter = new MessageFormat(labels.getString("comboBoxContractsAddedItem"), Locale.getDefault());
		for (RentalAgreement ra : contract_list)
		{
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			try
			{
				String strAddItem = formatter.format(new Object[] {ra.getId(), new SimpleDateFormat("yyyy-MM-dd").parse(ra.getDate()), sdf.parse(ra.getBegin_hour()), sdf.parse(ra.getEnd_hour())} );
				comboBoxContracts.addItem(strAddItem);
			}
			catch (Exception e) {
				App.logger.fatal("Parsing exception raised : " + e.getMessage());
				e.printStackTrace();
				comboBoxContracts.addItem(ra.getId() + " - " + ra.getDate() + " : " + ra.getBegin_hour() + " at " + ra.getEnd_hour());
			}			
		}	
	}
	
	/**
	 * Gives the id of the contract selected in the drop down list.
	 * @return The id of the contract selected, -1 if the 1st item of the list is selected.
	 * @throws NumberFormatException - If the code as a string is not convertible to int.
	 */
	public int getContractSelection()
	{
		if(comboBoxContracts.getSelectedIndex() == 0)
		{
			App.logger.warn("First index selected, returning -1");
			return -1;
		}
		else
		{
			String selection = (String) comboBoxContracts.getSelectedItem();
			String str_id_contract = selection.split(" ")[0];
			App.logger.debug("Id of the selected contract : " + str_id_contract);
			return Integer.parseInt(str_id_contract);	
		}
	}
	
	/**
	 * Get the renting amount of a boat depending on the contract time diff.
	 * Can throw ParseException if the begin_hour and the end_hour attribute of the rental agreement doesn't match the pattern 'HH:mm:ss' (see java.text.SimpleDateFormat).
	 * @param ra : The RentalAgreement to get the rent time.
	 * @param bt : The boat type to get the price depending on the time.
	 * @return total : The total to pay for the rent of this boat.
	 */
	public double getBoatRentPrice(RentalAgreement ra, BoatType bt)
	{
		App.logger.info("Calculating total rent amount of boat");
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
		double total = 0.0;
		try
		{
			Date begin_time = format.parse(ra.getBegin_hour());
			Date end_time = format.parse(ra.getEnd_hour());
			// result - Time diff in milliseconds
			long result = end_time.getTime() - begin_time.getTime();
			int nb_days = (int) result / 1000 / 60 / 60 / 24;
			int nb_half_days = (int) (result / 1000 / 60 / 60 / 12) - (nb_days * 2);
			int nb_hour = (int) (result / 1000 / 60 / 60) - (nb_half_days * 12);
			int nb_half_hour = (int) (result / 1000 / 60 / 30) - (nb_hour * 2);
			int nb_minutes = (int) (result / 1000 / 60) - (nb_half_hour * 30);
			if(nb_minutes > 0)
			{
				nb_half_hour += 1;
			}			
			total = nb_days * bt.getDailyPrice() + nb_half_days * bt.getHalfDailyPrice() + nb_hour * bt.getHourlyPrice() + nb_half_hour * bt.getHalfHourlyPrice();
			App.logger.debug("total amount of boat of type " + bt.getCode() + " : " + total + " euros");
		} catch (ParseException e) {
			App.logger.fatal("Time parsing exception raised : " + e.getMessage());
			e.printStackTrace();
		}
		return total;
	}

	private void populateTextFields(List<Rent> rent_list)
	{
		App.logger.info("Writing in the text fields");
		RentalAgreement contract = rent_list.get(0).getContract();
		
		MessageFormat formatter_date_fields = new MessageFormat(labels.getString("DateFields"), Locale.getDefault());		
		String str_end = "", str_begin = "";
		try
		{
			str_end = formatter_date_fields.format(new Object[] {new SimpleDateFormat("yyyy-MM-dd").parse(contract.getDate()),  new SimpleDateFormat("HH:mm:ss").parse(contract.getEnd_hour())});
			str_begin = formatter_date_fields.format(new Object[] {new SimpleDateFormat("yyyy-MM-dd").parse(contract.getDate()),  new SimpleDateFormat("HH:mm:ss").parse(contract.getBegin_hour())});
		} catch (ParseException e) {
			App.logger.fatal("Parsing exception raised : " + e.getMessage());
			e.printStackTrace();
		}
		
		textFieldBegin.setText(str_begin);
		textFieldEnd.setText(str_end);
		
		textFieldBoatNumber.setText(Integer.toString(rent_list.size()));
		String str = "";
		double total_price = 0;
		App.logger.info("Calculating total amount of contract");
		MessageFormat formatter_text_area = new MessageFormat(labels.getString("textAreaNewLine"), Locale.getDefault());
		for (Rent r : rent_list)
		{
			double boat_rent_price = this.getBoatRentPrice(contract, r.getRented_boat().getType());
			str += formatter_text_area.format(new Object[] {r.getRented_boat().getId(),  r.getRented_boat().getType().getName(), boat_rent_price} ) + "\n";			
			total_price += boat_rent_price;
		}
		App.logger.debug("Total amount : " + total_price);
		textAreaBoatsInfo.setText(str);
		textFieldAmount.setText(Double.toString(total_price) + " euros");
	}
}
