package fr.valentin.locaflot_facture;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import fr.valentin.locaflot_facture.CLI.CliManager;
import fr.valentin.locaflot_facture.Controller.MainController;
import fr.valentin.locaflot_facture.View.MainView;

import java.util.Locale;

public class App
{
	public static Logger logger = LogManager.getLogger();
    public static void main( String[] args )
    {
    	LocaleSetup(args);
    	logger.info("Actual locale : " + Locale.getDefault());
    	
    	MainController main_controller = new MainController();
		MainView frame = new MainView(main_controller);
		main_controller.setView(frame);
    }
    
    private static void LocaleSetup(String[] args)
    {
    	logger.debug("Setting up the localization...");
    	Locale.setDefault(new Locale("en", "US"));
    	if(args.length > 0)
    	{
    		CliManager manager = new CliManager(args);
    		String langage = manager.getLocale().toLowerCase();
    		if(langage.equals("fr") || langage.equals("french"))
    		{
    			Locale.setDefault(new Locale("fr", "FR"));
    		}
    		else
    		{
    			manager.PrintHelp();
    		}
    	}
    }
}
