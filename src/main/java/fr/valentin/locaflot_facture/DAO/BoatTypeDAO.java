package fr.valentin.locaflot_facture.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.valentin.locaflot_facture.Classes.BoatType;

/**
 * BoatTypeDAO : Class listing CRUD of the 'Type_Embarcation' table. 
 * @author Valentin Tazir
 */
public class BoatTypeDAO extends DAO<BoatType>
{

	/**
	 * Insert a tuple in the 'Type_Embarcation' table.
	 * @param obj : The BoatType object to insert in the database.
	 */
	@Override
	public void create(BoatType obj)
	{
		try {
		 	PreparedStatement prepare = this.connect
                                        .prepareStatement("INSERT INTO \"locaflot_facture\".type_embarcation VALUES(?, ?, ?, ?, ?, ?, ?)"
                                         );
		 	prepare.setString(1, obj.getCode());
			prepare.setString(2, obj.getName());
			prepare.setInt(3, obj.getSeatingCapacity());
			prepare.setDouble(4, obj.getHalfHourlyPrice());
			prepare.setDouble(5, obj.getHourlyPrice());
			prepare.setDouble(6, obj.getHalfDailyPrice());
			prepare.setDouble(7, obj.getDailyPrice());
			prepare.executeUpdate();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read a tuple.
	 * @param code : Primary key of the tuple of the 'Type_Embarcation' table.
	 * @return A BoatType object corresponding to the tuple.
	 */
	@Override
	public BoatType read(String code)
	{
		BoatType boat_type = new BoatType();
		try {
	          ResultSet result = this.connect
	                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                                                ResultSet.CONCUR_UPDATABLE)
	                             .executeQuery("SELECT * FROM \"locaflot_facture\".type_embarcation WHERE code = '" + code +"'");
	          if(result.first())
	        	  boat_type = new BoatType(code, result.getString("nom"), result.getInt("nb_place"), result.getDouble("prix_demi_heure"), result.getDouble("prix_heure"), result.getDouble("prix_demi_jour"), result.getDouble("prix_jour"));   
	    }
		catch (SQLException e) {
			        e.printStackTrace();
		}
		return boat_type;
	}

	/**
	 * Update the data of the 'Type_Embarcation' table.
	 * @param obj : The BoatType object to update in the database.
	 */
	@Override
	public void update(BoatType obj)
	{
		try {
		 	PreparedStatement prepare = this.connect
                                        .prepareStatement("UPDATE \"locaflot_facture\".type_embarcation SET nom = ?, nb_place = ?, prix_demi_heure = ?, prix_heure = ?, prix_demi_jour = ?, prix_jour = ? WHERE code = ?"
                                         );
			prepare.setString(1, obj.getName());
			prepare.setInt(2, obj.getSeatingCapacity());
			prepare.setDouble(3, obj.getHalfHourlyPrice());
			prepare.setDouble(4, obj.getHourlyPrice());
			prepare.setDouble(5, obj.getHalfDailyPrice());
			prepare.setDouble(6, obj.getDailyPrice());
			prepare.setString(7, obj.getCode());
			prepare.executeUpdate();

			obj = this.read(obj.getCode());
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Delete the data of the 'Type_Embarcation' table.
	 * @param obj : The BoatType object to delete in the database.
	 */
	@Override
	public void delete(BoatType obj)
	{
		try
		{
			this.connect
            .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
           		            ResultSet.CONCUR_UPDATABLE)
            .executeUpdate("DELETE FROM \"locaflot_facture\".louer WHERE id_embarcation IN (SELECT id FROM \"locaflot_facture\".embarcation WHERE type = '" + obj.getCode()+"')");
			
			this.connect
            .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
           		            ResultSet.CONCUR_UPDATABLE)
            .executeUpdate("DELETE FROM \"locaflot_facture\".embarcation WHERE type = '" + obj.getCode()+"'");
			
	         this.connect
	             .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		            ResultSet.CONCUR_UPDATABLE)
	             .executeUpdate("DELETE FROM \"locaflot_facture\".type_embarcation WHERE code = '" + obj.getCode()+"'");
		}
		catch (SQLException e) {
		     e.printStackTrace();
		}		
	}

	/**
	 * Read all the tuples of the 'Type_Embarcation' table
	 * @return A list of BoatType objects, similar to the database content.
	 */
	@Override
	public List<BoatType> recupAll()
	{
		List<BoatType> boat_type_list = new ArrayList<BoatType>();
		try
		{
			Statement query = this.connect.createStatement();
			ResultSet cursor = query.executeQuery("select * from \"locaflot_facture\".type_embarcation");

			while (cursor.next()){
				BoatType boat_type = new BoatType();
				boat_type.setCode(cursor.getString("code"));
				boat_type.setName(cursor.getString("nom"));
				boat_type.setSeatingCapacity(cursor.getInt("nb_place"));
				boat_type.setHalfHourlyPrice(cursor.getDouble("prix_demi_heure"));
				boat_type.setHourlyPrice(cursor.getDouble("prix_heure"));
				boat_type.setHalfDailyPrice(cursor.getDouble("prix_demi_jour"));
				boat_type.setDailyPrice(cursor.getDouble("prix_jour"));

				boat_type_list.add(boat_type);
			}
			cursor.close();
			query.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return boat_type_list; 
	}	
}
