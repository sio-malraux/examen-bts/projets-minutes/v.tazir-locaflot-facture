package fr.valentin.locaflot_facture.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.valentin.locaflot_facture.Classes.RentalAgreement;

/**
 * RentalAgreementDAO : Class listing CRUD of the 'Contrat_Location' table. 
 * @author Valentin Tazir
 */
public class RentalAgreementDAO extends DAO<RentalAgreement>{

	/**
	 * Insert a tuple in the 'Contrat_Location' table.
	 * @param obj : The RentalAgreement object to insert in the database.
	 */
	@Override
	public void create(RentalAgreement obj)
	{
		try
		{
		 	PreparedStatement prepare = this.connect
                                        .prepareStatement("INSERT INTO \"locaflot_facture\".contrat_location VALUES(?, ?, ?, ?)"
                                         );
		 	prepare.setInt(1, obj.getId());
			prepare.setString(2, obj.getDate());
			prepare.setString(3, obj.getBegin_hour());
			prepare.setString(4, obj.getEnd_hour());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read a tuple.
	 * @param code : Primary key of the tuple of the 'Contrat_Location' table.
	 * @return A RentalAgreement object corresponding to the tuple.
	 */
	@Override
	public RentalAgreement read(String code)
	{
		int id = Integer.parseInt(code);
		RentalAgreement rental_agreement = new RentalAgreement();
		try
		{
	          ResultSet result = this.connect
	                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                                                ResultSet.CONCUR_UPDATABLE)
	                             .executeQuery("SELECT * FROM \"locaflot_facture\".contrat_location WHERE id = " + id + ";");
	          if(result.first())
	        	  rental_agreement = new RentalAgreement(id, result.getString("date"), result.getString("heure_debut"), result.getString("heure_fin"));   
	    } catch (SQLException e) {
			        e.printStackTrace();
		}
		return rental_agreement;
	}

	/**
	 * Update the data of the 'Contrat_Location' table.
	 * @param obj : The RentalAgreement object to update in the database.
	 */
	@Override
	public void update(RentalAgreement obj)
	{
		try
		{
		 	PreparedStatement prepare = this.connect
                                        .prepareStatement("UPDATE \"locaflot_facture\".contrat_location SET date = ?, heure_debut = ?, heure_fin = ? WHERE id = ?"
                                         );
			prepare.setString(1, obj.getDate());
			prepare.setString(2, obj.getBegin_hour());
			prepare.setString(3, obj.getEnd_hour());
			prepare.setInt(4, obj.getId());
			prepare.executeUpdate();

			obj = this.read(Integer.toString(obj.getId()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Delete the data of the 'Contrat_Location' table.
	 * @param obj : The RentalAgreement object to delete in the database.
	 */
	@Override
	public void delete(RentalAgreement obj)
	{
		try
		{
			this.connect
            .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
           		            ResultSet.CONCUR_UPDATABLE)
            .executeUpdate("DELETE FROM \"locaflot_facture\".louer WHERE id_contrat = " + obj.getId() + ";");
			
	         this.connect
	             .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		            ResultSet.CONCUR_UPDATABLE)
	             .executeUpdate("DELETE FROM \"locaflot_facture\".contrat_location WHERE id = " + obj.getId() + ";");		
		} catch (SQLException e) {
		     e.printStackTrace();
		}		
	}

	/**
	 * Read all the tuples of the 'Contrat_Location' table
	 * @return A list of RentalAgreement objects, similar to the database content.
	 */
	@Override
	public List<RentalAgreement> recupAll()
	{
		List<RentalAgreement> list_rental_agreement = new ArrayList<RentalAgreement>();
		try
		{
			Statement query = this.connect.createStatement();
			ResultSet cursor = query.executeQuery("select * from \"locaflot_facture\".contrat_location");

			while (cursor.next()){
				RentalAgreement rental_agreement = new RentalAgreement();
				rental_agreement.setId(cursor.getInt("id"));
				rental_agreement.setDate(cursor.getString("date"));
				rental_agreement.setBegin_hour(cursor.getString("heure_debut"));
				rental_agreement.setEnd_hour(cursor.getString("heure_fin"));

				list_rental_agreement.add(rental_agreement);
			}
			cursor.close();
			query.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list_rental_agreement;
	}
}
