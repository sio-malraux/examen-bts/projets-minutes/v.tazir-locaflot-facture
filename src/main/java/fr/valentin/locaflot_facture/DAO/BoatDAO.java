package fr.valentin.locaflot_facture.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.valentin.locaflot_facture.Classes.Boat;

/**
 * BoatDAO : Class listing CRUD of the 'Embarcation' table. 
 * @author Valentin Tazir
 */
public class BoatDAO extends DAO<Boat>
{
	/**
	 * Insert a tuple in the 'Embarcation' table.
	 * @param obj : The boat object to insert in the database.
	 */
	@Override
	public void create(Boat obj)
	{
		try
		{
		 	PreparedStatement prepare = this.connect
                                        .prepareStatement("INSERT INTO \"locaflot_facture\".embarcation VALUES(?, ?, ?, ?)"
                                         );
		 	prepare.setString(1, obj.getId());
			prepare.setString(2, obj.getColor());
			prepare.setBoolean(3, obj.isAvailable());
			prepare.setString(4, obj.getType().getCode());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}

	/**
	 * Read a tuple.
	 * @param code : Primary key of the tuple of the 'Embarcation' table.
	 * @return A Boat object corresponding to the tuple.
	 */
	@Override
	public Boat read(String code)
	{
		Boat boat = new Boat();
		try
		{
	          ResultSet result = this.connect
	                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                                                ResultSet.CONCUR_UPDATABLE)
	                             .executeQuery("SELECT * FROM \"locaflot_facture\".embarcation WHERE id = '" + code + "'");
	          if(result.first())
	          {
	        	  BoatTypeDAO boat_type_dao = new BoatTypeDAO();
	        	  boat = new Boat(code, result.getString("couleur"), result.getBoolean("disponible"), boat_type_dao.read(result.getString("type")));
	          }
	    } catch (SQLException e) {
			  e.printStackTrace();
		}		
		return boat;
	}

	/**
	 * Update the data of the 'Embarcation' table.
	 * @param obj : The Boat object to update in the database.
	 */
	@Override
	public void update(Boat obj)
	{
		try
		{
		 	PreparedStatement prepare = this.connect
                                        .prepareStatement("UPDATE \"locaflot_facture\".embarcation SET couleur = ?, disponible = ?, type = ? WHERE id = ?"
                                         );
			prepare.setString(1, obj.getColor());
			prepare.setBoolean(2, obj.isAvailable());
			prepare.setString(3, obj.getType().getCode());
			prepare.setString(4, obj.getId());
			prepare.executeUpdate();

			obj = this.read(obj.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Delete the data of the 'Embarcation' table.
	 * @param obj : The Boat object to delete in the database.
	 */
	@Override
	public void delete(Boat obj)
	{
		try
		{
			this.connect
            .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
           		            ResultSet.CONCUR_UPDATABLE)
            .executeUpdate("DELETE FROM \"locaflot_facture\".louer WHERE id_embarcation = '" + obj.getId() + "'");
			
	         this.connect
	             .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		            ResultSet.CONCUR_UPDATABLE)
	             .executeUpdate("DELETE FROM \"locaflot_facture\".embarcation WHERE id = '" + obj.getId() + "'");		
		} catch (SQLException e) {
		     e.printStackTrace();
		}
		
	}

	/**
	 * Read all the tuples of the 'Embarcation' table
	 * @return A list of Boat objects, similar to the database content.
	 */
	@Override
	public List<Boat> recupAll()
	{
		List<Boat> list_boat = new ArrayList<Boat>();
		try
		{
			Statement query = this.connect.createStatement();
			ResultSet cursor = query.executeQuery("select * from \"locaflot_facture\".embarcation");

			while (cursor.next()){
				Boat boat = new Boat();
				BoatTypeDAO boat_type_dao = new BoatTypeDAO();
				boat.setId(cursor.getString("id"));
				boat.setColor(cursor.getString("couleur"));
				boat.setAvailable(cursor.getBoolean("disponible"));
				boat.setType(boat_type_dao.read(cursor.getString("type")));

				list_boat.add(boat);
			}
			cursor.close();
			query.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list_boat;
	}
}
