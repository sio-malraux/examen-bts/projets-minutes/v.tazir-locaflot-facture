package fr.valentin.locaflot_facture.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * DatabaseConnection : Class used to manage database connection
 * @author Valentin Tazir
 */
public class DatabaseConnection
{
	//private static String url = "jdbc:postgresql://postgresql.bts-malraux72.net:5432/v.tazir";
	private static String url = "jdbc:postgresql://localhost:1999/v.tazir";
	private static String user = "v.tazir";
	private static String password = "P@ssword";
	private static Connection connect = null;

	/**
	 * Connect to the database.
	 * @return connect : Database handler.
	 */
	public static Connection getInstance()
	{
		if (connect == null)
		{
			try
			{
				connect = DriverManager.getConnection(url, user, password);
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return connect;
	}

	/**
	 * Disconnect the database
	 */
	 public static void Disconnect()
	 {
		try
		{
			connect.close();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
}
