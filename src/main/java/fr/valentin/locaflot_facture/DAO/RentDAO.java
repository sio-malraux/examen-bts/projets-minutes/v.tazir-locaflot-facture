package fr.valentin.locaflot_facture.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.valentin.locaflot_facture.Classes.Rent;

/**
 * RentDAO : Class listing CRUD of the 'Louer' table. 
 * @author Valentin Tazir
 */
public class RentDAO extends DAO<Rent>
{
	
	/**
	 * Insert a tuple in the 'Louer' table.
	 * @param obj : The Rent object to insert in the database.
	 */
	@Override
	public void create(Rent obj)
	{
		try
		{
		 	PreparedStatement prepare = this.connect
                                        .prepareStatement("INSERT INTO \"locaflot_facture\".louer VALUES(?, ?, ?)"
                                         );
		 	prepare.setInt(1, obj.getContract().getId());
			prepare.setString(2, obj.getRented_boat().getId());
			prepare.setInt(3, obj.getNb_persons());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}

	/**
	 * Unused.
	 * @return null.
	 */
	@Override
	public Rent read(String code)
	{
		return null;
	}
	
	/**
	 * Read a tuple.
	 * @param contract_id : Primary key of the tuple of the 'Louer' table.
	 * @param boat_id : Primary key of the tuple of the 'Louer' table.
	 * @return A Rent object corresponding to the tuple.
	 */
	public Rent read(int contract_id, String boat_id)
	{
		Rent rent = new Rent();
		try
		{
	          ResultSet result = this.connect
	                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                                                ResultSet.CONCUR_UPDATABLE)
	                             .executeQuery("SELECT * FROM \"locaflot_facture\".louer WHERE id_contrat = " + contract_id + " AND id_embarcation = '" + boat_id + "'");
	          if(result.first())
	          {
	        	  BoatDAO boat_dao = new BoatDAO();
	        	  RentalAgreementDAO rental_agreement_dao = new RentalAgreementDAO();
	        	  rent = new Rent(rental_agreement_dao.read(Integer.toString(contract_id)), boat_dao.read(boat_id), result.getInt("nb_personnes"));
	          }
	    } catch (SQLException e) {
			  e.printStackTrace();
		}		
		return rent;
	}

	/**
	 * Update the data of the 'Louer' table.
	 * @param obj : The Rent object to update in the database.
	 */
	@Override
	public void update(Rent obj)
	{
		try
		{
		 	PreparedStatement prepare = this.connect
                                        .prepareStatement("UPDATE \"locaflot_facture\".louer SET nb_personnes = ? WHERE id_contrat = ? AND id_embarcation = ?"
                                         );
			prepare.setInt(1, obj.getNb_persons());
			prepare.setInt(2, obj.getContract().getId());
			prepare.setString(3, obj.getRented_boat().getId());
			prepare.executeUpdate();

			obj = this.read(obj.getContract().getId(), obj.getRented_boat().getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}

	/**
	 * Delete the data of the 'Louer' table.
	 * @param obj : The Rent object to delete in the database.
	 */
	@Override
	public void delete(Rent obj)
	{
		try
		{
	         this.connect
	             .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		            ResultSet.CONCUR_UPDATABLE)
	             .executeUpdate("DELETE FROM \"locaflot_facture\".louer WHERE id_contrat = " + obj.getContract().getId() + " AND id_embarcation = '" + obj.getRented_boat().getId() + "'");
		} catch (SQLException e) {
		     e.printStackTrace();
		}		
	}

	/**
	 * Retrieve all the data of the 'Louer' table corresponding to a rental contract.
	 * @param contract_id : The id of the rental contract.
	 * @return A list of Rent objects, related to a rental contract.
	 */
	public List<Rent> recupAllRentOfContract(int contract_id)
	{
		List<Rent> list_rent = new ArrayList<Rent>();		
		for(Rent r : this.recupAll())
		{
			if(r.getContract().getId() == contract_id)
			{
				list_rent.add(r);
			}
		}		
		return list_rent;
	}
	
	/**
	 * Read all the tuples of the 'Louer' table
	 * @return A list of Rent objects, similar to the database content.
	 */
	@Override
	public List<Rent> recupAll()
	{
		List<Rent> list_rent = new ArrayList<Rent>();
		try
		{
			Statement query = this.connect.createStatement();
			ResultSet cursor = query.executeQuery("select * from \"locaflot_facture\".louer");

			while (cursor.next()){
				Rent rent = new Rent();
				BoatDAO boat_dao = new BoatDAO();
				RentalAgreementDAO rental_agreement_dao = new RentalAgreementDAO();
				rent.setContract(rental_agreement_dao.read(Integer.toString(cursor.getInt("id_contrat"))));
				rent.setRented_boat(boat_dao.read(cursor.getString("id_embarcation")));
				rent.setNb_persons(cursor.getInt("nb_personnes"));

				list_rent.add(rent);
			}
			cursor.close();
			query.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return list_rent;
	}
}
