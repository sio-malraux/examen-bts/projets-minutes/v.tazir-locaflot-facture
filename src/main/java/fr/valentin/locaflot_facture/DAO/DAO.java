package fr.valentin.locaflot_facture.DAO;

import java.sql.Connection;
import java.util.List;

/**
 * DAO class : Abstract class listing CRUD methods. 
 * @author Valentin Tazir
 */
public abstract class DAO<T>
{
	/**
	 * Database connection Handler.
	 */
	public Connection connect = DatabaseConnection.getInstance();
	
	/**
	 * Insert a tuple.
	 * @param obj : The object to insert in the database.
	 */
	public abstract void create(T obj);
	
	/**
	 * Read a tuple.
	 * @param code : Primary key of the tuple of the related table.
	 * @return An object corresponding to the tuple.
	 */
	public abstract T read(String code);
	
	/**
	 * Update the data of a table.
	 * @param obj : The object to update in the database.
	 */
	public abstract void update(T obj);
	
	/**
	 * Delete the data of a table.
	 * @param obj : The object to delete in the database.
	 */
	public abstract void delete(T obj);
	
	/**
	 * Read all the tuples of a table
	 * @return A list of objects.
	 */
	public abstract List<T> recupAll();
}
