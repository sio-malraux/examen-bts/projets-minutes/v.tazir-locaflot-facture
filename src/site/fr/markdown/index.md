# `Page d'accueil de Locaflot-facture`

## `Qu'est-ce que 'Locaflot-facture' ?`

Locaflot-facture est une application [_apache maven_](http://maven.apache.org/) crée pour permettre à la société __Locaflot__ de générer ses factures, ce qui permet de visualiser le montant de chaque contrat de manière plus simple.  
Les informations générées par les factures sont :

- L'horodatage de la location,
- Toutes les informations sur les bateaux loués (type de bateau, numéro d'identification du bateau, montant du bateau loué),
- Le nombre de bateaux loués,
- Le montant total du contrat.

La facture peut être consulté à partir de l'application ou d'un fichier PDF généré par l'application.

## `Diagramme de classe du projet`

![Diagramme UML des classes](../img/uml.png)

## `Javadoc`

Vous pouvez consulter la documentation technique [ici](apidocs/index.html). 

## `Tutorial`

Avant de faire quoi que ce soit, vérifiez que vous avez bien installé [_java_](https://www.java.com/download/) sur votre ordinateur.

Tout d'abord, vous devez télécharger et extraire l'application (artefact 'livraison') disponible sur le [_dépôt git_](https://framagit.org/sio-malraux/examen-bts/projets-minutes/v.tazir-locaflot-facture).

Une fois que vous avez extrait l'application, vous pouvez la lancer en entrant cette commande :

```
java -jar locaflot-facture-0.0.1-SNAPSHOT.jar [-l <langage>]
```

L'option *-l* ou *--locale* permet à l'utilisateur de changer la langue utilisée par l'application.  
Les arguments actuellement acceptés sont :

- **FR**, **French**
    
- <u>Par défaut :</u> Langue **Anglaise** si l'option *-l* n'est pas renseigné.

Ensuite, choisissez le contrat dont vous voulez voir la facture dans la liste déroulante et cliquez sur le bouton __Générer__ pour générer la facture. Vous verrez les informations concernant le contrat sélectionné.  
Vous pouvez choisir de générer un fichier PDF contenant les informations en cliquant sur le bouton __PDF__.   
Si vous souhaitez consulter une autre facture, vous pouvez appuyer sur le bouton __Retour__ et répéter la tâche précédente.
