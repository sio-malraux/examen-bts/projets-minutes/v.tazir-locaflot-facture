# `Locaflot-facture Homepage`

[French website](fr/index.html)

## `What's 'Locaflot-facture' ?`

Locaflot-facture is a [_apache maven_](http://maven.apache.org/) app made to allow the __Locaflot__ to generate his bills, making possible to view the amount of every contract in a more friendly way.  
The informations the bills are generated are :

- Date and time of rent
- Every infos about the rented boats (Boat type, Boat id, amount of the rent)
- Number of rented boats
- Total amount of the contract

The bill can be viewed from the app or a PDF file generated by the app.

## `Class diagram of the project`

![Diagramme UML des classes](img/uml.png)

## `Javadoc`

You can access the technical documentation of the project [here](apidocs/index.html). 

## `Tutorial`

Before doing anything, verify you have [_java_](https://www.java.com/download/) installed on your computer.

First of all, you need to download and extract the app (artifact 'livraison') available on the [_git repository_](https://framagit.org/sio-malraux/examen-bts/projets-minutes/v.tazir-locaflot-facture).

Once you've extracted the app, you can launch the app by doing this command :

```
java -jar locaflot-facture-0.0.1-SNAPSHOT.jar [-l <langage>]
```

The *-l* or *--locale* option allow the user to change the language used by the app.  
Currently accepted arguments are : 

- **FR**, **French**
    
- <u>Default :</u> **English** language if the *--locale* option is not typed.

Then, choose the contract you want to see the bill in the drop down list and click the __Generate__ button to generate the bill. You will see the infos concerning the contract selected.  
You can choose to generate a PDF file containing the infos by clicking the __PDF__ button.  
If you want to view another bill, you can press the button __Return__ and repeat the previous task.
