package fr.valentin.locaflot_facture;

import fr.valentin.locaflot_facture.Classes.Boat;
import fr.valentin.locaflot_facture.Classes.BoatType;
import fr.valentin.locaflot_facture.Classes.Rent;
import fr.valentin.locaflot_facture.Classes.RentalAgreement;
import junit.framework.TestCase;

public class RentTest extends TestCase
{
	public void testConstructors()
	{
		try
		{
			Rent r = new Rent();
			Rent r2 = new Rent(new RentalAgreement(), new Boat(), 0);
		} catch (Exception e) {
			fail("Rent constructors throw exception");
		}
	}
	
	public void testGetContract()
	{
		RentalAgreement ra = new RentalAgreement(14576210, "10/10/2020", "13:00:00", "20:00:00");
		Rent r = new Rent(ra, new Boat(), 0);
		assertSame(ra, r.getContract());
	}
	
	public void testGetRentedBoat()
	{
		Boat b = new Boat("1456-213M", "red", false, new BoatType());
		Rent r = new Rent(new RentalAgreement(), b, 0);
		assertSame(b, r.getRented_boat());
	}
	
	public void testGetNbPersons()
	{
		Rent r = new Rent(new RentalAgreement(), new Boat(), 264);
		assertEquals(264, r.getNb_persons());
	}
	
	public void testSetContract()
	{
		RentalAgreement ra = new RentalAgreement(159357, "13/05/2020", "08:00:00", "19:30:00");
		Rent r = new Rent();
		r.setContract(ra);
		assertSame(ra, r.getContract());
	}
	
	public void testSetRentedBoat()
	{
		Boat b = new Boat("123-MNE-91B", "yellow", true, new BoatType());
		Rent r = new Rent();
		r.setRented_boat(b);
		assertSame(b, r.getRented_boat());
	}
	
	public void testSetNbPersons()
	{
		Rent r = new Rent();
		r.setNb_persons(12456789);
		assertEquals(12456789, r.getNb_persons());
	}
}
