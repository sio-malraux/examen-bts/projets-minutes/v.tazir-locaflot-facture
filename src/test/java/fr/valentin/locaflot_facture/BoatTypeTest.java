package fr.valentin.locaflot_facture;

import fr.valentin.locaflot_facture.Classes.BoatType;
import junit.framework.TestCase;

public class BoatTypeTest extends TestCase
{
	public void testConstructors()
	{
		try
		{
			BoatType bt = new BoatType();
			BoatType bt2 = new BoatType("", "", 0, 0.0, 0.0, 0.0, 0.0);	
		}catch (Exception e) {
			fail("Contructors throw exception");
		}
	}
	
	public void testGetCode()
	{
		BoatType bt = new BoatType("azerty", "", 0, 0, 0, 0, 0);
		assertTrue(bt.getCode().equals("azerty"));
	}
	
	public void testGetName()
	{
		BoatType bt = new BoatType("", "test", 0, 0.0, 0.0, 0.0, 0.0);
		assertTrue(bt.getName().equals("test"));
	}
	
	public void testGetSeatingCapacity()
	{
		BoatType bt = new BoatType("", "", 8, 0.0, 0.0, 0.0, 0.0);
		assertTrue(bt.getSeatingCapacity() == 8);
	}
	
	public void testGetHalfHourlyPrice()
	{
		BoatType bt = new BoatType("", "", 0, 13.22, 0.0, 0.0, 0.0);
		assertTrue(bt.getHalfHourlyPrice() == 13.22);
	}
	
	public void testGetHourlyPrice()
	{
		BoatType bt = new BoatType("", "", 0, 0.0, 140.24, 0.0, 0.0);
		assertTrue(bt.getHourlyPrice() == 140.24);
	}
	
	public void testGetHalfDailyPrice()
	{
		BoatType bt = new BoatType("", "", 0, 0.0, 0.0, 150.186, 0.0);
		assertTrue(bt.getHalfDailyPrice() == 150.186);
	}
	
	public void testGetDailyPrice()
	{
		BoatType bt = new BoatType("", "", 0, 0.0, 0.0, 0.0, 200.6852);
		assertTrue(bt.getDailyPrice() == 200.6852);
	}
	
	public void testSetCode()
	{
		BoatType bt = new BoatType();
		bt.setCode("1234");
		assertTrue(bt.getCode().equals("1234"));
	}
	
	public void testSetName()
	{
		BoatType bt = new BoatType();
		bt.setName("azerty");
		assertTrue(bt.getName().equals("azerty"));
	}
	
	public void testSetSeatingCapacity()
	{
		BoatType bt = new BoatType();
		bt.setSeatingCapacity(16);
		assertEquals(16, bt.getSeatingCapacity());
	}
	
	public void testSetHalfHourlyPrice()
	{
		BoatType bt = new BoatType();
		bt.setHalfHourlyPrice(2145.369);
		assertEquals(2145.369, bt.getHalfHourlyPrice());
	}
	
	public void testSetHourlyPrice()
	{
		BoatType bt = new BoatType();
		bt.setHourlyPrice(12345.987456);
		assertEquals(12345.987456, bt.getHourlyPrice());
	}
	
	public void testSetHalfDailyPrice()
	{
		BoatType bt = new BoatType();
		bt.setHalfDailyPrice(14725.8369);
		assertEquals(14725.8369, bt.getHalfDailyPrice());
	}
	
	public void testSetDailyPrice()
	{
		BoatType bt = new BoatType();
		bt.setDailyPrice(7890145.32165901);
		assertEquals(7890145.32165901, bt.getDailyPrice());
	}
}
