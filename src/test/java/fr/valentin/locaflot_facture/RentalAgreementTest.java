package fr.valentin.locaflot_facture;

import fr.valentin.locaflot_facture.Classes.RentalAgreement;
import junit.framework.TestCase;

public class RentalAgreementTest extends TestCase
{
	public void testConstructors()
	{
		try
		{
			RentalAgreement ra = new RentalAgreement();
			RentalAgreement ra2 = new RentalAgreement(0, "", "", "");	
		} catch (Exception e) {
			fail("RentalAgreement contructors throw exception");
		}
	}
	
	public void testGetId()
	{
		RentalAgreement ra = new RentalAgreement(123456, "", "", "");
		assertEquals(123456, ra.getId());
	}
	
	public void testGetDate()
	{
		RentalAgreement ra = new RentalAgreement(0, "25/8a/erty", "", "");
		assertTrue(ra.getDate().equals("25/8a/erty"));
	}
	
	public void testGetBeginHour()
	{
		RentalAgreement ra = new RentalAgreement(0, "", "12:00:45", "");
		assertTrue(ra.getBegin_hour().equals("12:00:45"));
	}
	
	public void testGetEndHour()
	{
		RentalAgreement ra = new RentalAgreement(0, "", "", "23:45:18");
		assertTrue(ra.getEnd_hour().equals("23:45:18"));
	}
	
	public void testSetId()
	{
		RentalAgreement ra = new RentalAgreement();
		ra.setId(123698);
		assertEquals(123698, ra.getId());
	}
	
	public void testSetDate()
	{
		RentalAgreement ra = new RentalAgreement();
		ra.setDate("10/10/2010");
		assertTrue(ra.getDate().equals("10/10/2010"));
	}
	
	public void testSetBeginHour()
	{
		RentalAgreement ra = new RentalAgreement();
		ra.setBegin_hour("10:14:20");
		assertTrue(ra.getBegin_hour().equals("10:14:20"));
	}
	
	public void testSetEndHour()
	{
		RentalAgreement ra = new RentalAgreement();
		ra.setEnd_hour("20:14:08");
		assertTrue(ra.getEnd_hour().equals("20:14:08"));
	}
}
