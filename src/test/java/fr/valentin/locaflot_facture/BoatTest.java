package fr.valentin.locaflot_facture;

import fr.valentin.locaflot_facture.Classes.Boat;
import fr.valentin.locaflot_facture.Classes.BoatType;
import junit.framework.TestCase;

public class BoatTest extends TestCase
{
	public void testConstructor()
	{
		try
		{
			Boat b = new Boat();
			Boat b2 = new Boat("", "", false, null);
		} catch (Exception e) {
			fail("Boat contructors throw exception");
		}		
	}
	
	public void testGetId()
	{
		Boat b = new Boat("azertyuiop", "", false, null);
		assertTrue(b.getId().equals("azertyuiop"));
	}
	
	public void testGetColor()
	{
		Boat b = new Boat("", "qsdfghjklm", false, null);
		assertTrue(b.getColor().equals("qsdfghjklm"));
	}
	
	public void testIsAvailable()
	{
		Boat b = new Boat("", "", true, null);
		assertEquals(true, b.isAvailable());
	}
	
	public void testGetType()
	{
		BoatType bt = new BoatType("az", "mlkjgfdrb", 0, 0.0, 0.0, 0.2, 0.14);
		Boat b = new Boat("", "", false, bt);
		assertSame(bt, b.getType());
	}
	
	public void testSetId()
	{
		Boat b = new Boat();
		b.setId("123456789AZERTY");
		assertTrue("123456789AZERTY".equals(b.getId()));
	}
	
	public void testSetColor()
	{
		Boat b = new Boat();
		b.setColor("purple");
		assertTrue("purple".equals(b.getColor()));
	}
	
	public void testSetAvailable()
	{
		Boat b = new Boat();
		b.setAvailable(true);
		assertEquals(true, b.isAvailable());
	}
	
	public void testSetType()
	{
		BoatType bt = new BoatType("14", "Barque", 5, 13.22, 20.2, 40.36, 70.54);
		Boat b = new Boat();
		b.setType(bt);
		assertSame(bt, b.getType());
	}
}
